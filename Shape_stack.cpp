// Shape_stack.cpp

#include "Shapes.h"
#include "Grid.h"
#include "Shape_stack.h"
#include <stdexcept>
#include <utility>
#include <array>

Shape_stack::Shape_stack()
{
}

Shape_stack::Shape_stack(const Shape_stack &other)
    : m_active_shape{ other.m_active_shape }, 
    m_shape_colour{ other.m_shape_colour }, 
    m_grid{ other.m_grid }
{
}

Shape_stack::Shape_stack(Shape_stack &&other) : Shape_stack()
{
    swap(*this, other);
}

Shape_stack::Shape_stack(const int rows, const int cols)
    : m_grid{ rows, cols }
{
}

Shape_stack& Shape_stack::operator=(Shape_stack other)
{
    swap(*this, other);
    return *this;
}

std::array<Coord, 4> Shape_stack::active_shape_coords() const
{
    std::array<Coord, 4> coords;
    for (int index = 0; index < 4; ++index)
        coords[index] = m_active_shape.get_coords()[index] +
             m_shape_pos;
    
    return coords;
}

void Shape_stack::set_shape_fill(bool flag)
{
    try {
        for (const Coord &coord : m_active_shape.get_coords()) {
            Block &shape_block = 
                m_grid.at(m_shape_pos.row() + coord.row(),
                          m_shape_pos.col() + coord.col());
            shape_block.set_fill(flag);
        }
    }
    catch (std::out_of_range) {
        throw;
    }
}

// returns true if move successfull, false otherwise
bool Shape_stack::can_move_to(const Coord &new_coord)
{
    try {
        for (const Coord &shape_coord : m_active_shape.get_coords()) {
            Coord block_coord { new_coord + shape_coord };
            int row { static_cast<int>(block_coord.row()) };
            int col { static_cast<int>(block_coord.col()) };
            if (m_grid.at(row, col).is_full()) return false;
        } 
    }
    catch(std::out_of_range &) {
        return false;
    }
    return true;
}

void Shape_stack::move_shape(const Coord &new_coord)
{
    m_shape_pos = new_coord;
}

void Shape_stack::move_right()
{
    Coord new_coord { m_shape_pos + Coord{ 0, 1 } };
    if (can_move_to(new_coord))
        move_shape(new_coord);
}

void Shape_stack::move_left()
{
    Coord new_coord { m_shape_pos + Coord{ 0, -1 } };
    if (can_move_to(new_coord))
        move_shape(new_coord);
}

bool Shape_stack::move_down()
{
    Coord new_coord { m_shape_pos + Coord{ 1, 0 } };
    if (can_move_to(new_coord)) {
        move_shape(new_coord);
        return true;
    }
    else return false;
}

// return give success
bool Shape_stack::add_shape(const Shape &shape, const Coord &at_coord)
{
    m_active_shape = shape;
    if (can_move_to(at_coord)) {
        move_shape(at_coord);
        return true;
    }
    else return false;
}

void Shape_stack::rotate_shape()
{
    Shape backup_shape{ m_active_shape };
    m_active_shape.rotate();
    // find out how far the shape has potentially moved off the grid
    Coord offset{ 0, 0 };
    for (const Coord& shape_coord : m_active_shape.get_coords()) { 
        Coord block_coord { shape_coord + m_shape_pos };
        int block_row { static_cast<int>(block_coord.row()) }; 
        int block_col { static_cast<int>(block_coord.col()) };
        if (block_col < 0 && block_col < offset.col()) 
            offset.col() = block_col;
        else if (block_col > m_grid.width() - 1 &&
                 block_col - (m_grid.width() - 1) > offset.col())
            offset.col() = block_col - (m_grid.width() - 1);
        if (block_row < 0 && block_row < offset.row())
            offset.row() = block_row;
    }

    // attempt to move the shape back onto the grid, don't rotate if 
    // not possible
    if (can_move_to(m_shape_pos - offset))
        move_shape(m_shape_pos - offset);
    else m_active_shape = backup_shape;
}     

// only checks where the shape currently is as these can be the only
// completed rows
void Shape_stack::update_rows()
{
    int row_to_check;
    int bottom_row_removed{ 0 };
    int rows_removed{ 0 };
    for (auto &coord : m_active_shape.get_coords()) {
        // check to see if row already deleted
        bool row_complete{ true };
        if (m_grid.at(m_shape_pos.row() + coord.row(),
                    m_shape_pos.col() + coord.col()).is_full()) {
            row_to_check = m_shape_pos.row() + coord.row();
            for (int col = 0; col < m_grid.width(); col++) {
                if (!m_grid.at(row_to_check, col).is_full()) {
                    row_complete = false;
                    break;
                }
            }
            if (row_complete) {
                m_grid.empty_row(row_to_check);
                rows_removed += 1;
                if (row_to_check > bottom_row_removed) 
                    bottom_row_removed = row_to_check;
            }
        }
    }
    
    // now move all the rows above the most bottom row down
    if (rows_removed > 0) {
        for (int row_to_move = bottom_row_removed - rows_removed;
             row_to_move >= 0; row_to_move--)
            m_grid.shift_row_down(row_to_move, rows_removed);
    }
}

void swap(Shape_stack &first, Shape_stack &second)
{
    swap(first.m_active_shape, second.m_active_shape);
    std::swap(first.m_shape_colour, second.m_shape_colour);
    swap(first.m_grid, second.m_grid);
}
