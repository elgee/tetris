/* Player.h
 * player class which holds the information about each player, eg. score,
 * keybindings, etc. Each player has their own shape stack wich is accessed
 * through the player
 */
#ifndef PLAYER_H
#define PLAYER_H

#include "Shape_stack.h"

class Player {
public:
    Player(const int stack_rows, const int stack_cols)
        : m_stack{ stack_rows, stack_cols }
    {
    }
    
    void set_key_down(const int key) { m_key_down = key; }
    void set_key_left(const int key) { m_key_left = key; }
    void set_key_right(const int key) { m_key_right = key; }
    void set_key_rotate(const int key) { m_key_rotate = key; }
    int get_score() { return m_score; }
    void score(const int points) { m_score += points; }
    Shape_stack& stack() { return m_stack; }
private:
    int m_score = 0;
   
    int m_key_down;
    int m_key_left;
    int m_key_right;
    int m_key_rotate;
    Shape_stack m_stack;
    
};

#endif
