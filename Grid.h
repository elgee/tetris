/* Grid.h
 * object which holds the coordinate system and tiles
 */
#ifndef GRID_H
#define GRID_H

#include <vector>
#include "Shapes.h"

enum class Colour;
class Block;

class Grid {
public: 
    Grid();
    Grid(const Grid &other);
    Grid(Grid &&other);
    Grid(const int rows, const int cols);
    Grid& operator=(Grid other);
    
    // element access
    Block& at(const int row, const int col);
    const Block& at(const int row, const int col) const;

    int width() const;
    int height() const;
    bool is_full(const int row, const int col) const;
    void empty_block(const int row, const int col);
    void empty_row(const int row);
    void shift_row_down(const int row, const int shift = 1);
    
    void add_shape(const Shape &shape, const int row, const int col);

    friend void swap(Grid &first, Grid &second);
private:
    int m_rows = 0;
    int m_cols = 0;
    std::vector<Block> m_blocks;
};

enum class Colour {
    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN,
    WHITE,
};
 
class Block {
public:
    Colour get_colour() const;
    void set_colour(Colour colour);
    bool is_full() const;
    void set_fill(bool flag);
private:
    Colour m_colour = Colour::BLACK;
    bool m_filled = false;
};

#endif
