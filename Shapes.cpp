#include "Shapes.h"
#include <array>
#include <utility>

Coord::Coord()
{
}

Coord::Coord(const Coord &other) : m_coords{ other.m_coords }
{
}

Coord::Coord(Coord &&other) : Coord()
{
    swap(*this, other);
}

Coord::Coord(const double row, const double col) : m_coords{ row, col }
{
}

Coord& Coord::operator=(Coord other)
{
    swap(*this, other);
    return *this;
}

Coord& Coord::operator+=(const Coord &rhs)
{
    m_coords[0] += rhs.m_coords[0];
    m_coords[1] += rhs.m_coords[1];
    return *this;
}

Coord& Coord::operator-=(const Coord &rhs)
{
    m_coords[0] -= rhs.m_coords[0];
    m_coords[1] -= rhs.m_coords[1];
    return *this;
}

void swap(Coord &first, Coord &second)
{
    std::swap(first.m_coords, second.m_coords);
}

Coord operator+(Coord lhs, const Coord &rhs)
{
    return lhs += rhs;
}

Coord operator-(Coord lhs, const Coord &rhs)
{
    return lhs -= rhs;
}

Shape::Shape()
{
}

Shape::Shape(std::array<Coord, 4> &coords) : m_coords{ coords }
{
}

Shape::Shape(std::array<Coord, 4> &&coords) 
    : m_coords{ std::move(coords) } 
{
}

Shape& Shape::operator=(Shape other)
{
    swap(*this, other);
    return *this;
}

void Shape::rotate()
{
    for (Coord &coord : m_coords){
        Coord old = coord;
        coord.col() = -old.row();
        coord.row() = old.col();
    }
};

const std::array<Coord, 4> Shape::get_coords() const { return m_coords; }

void swap(Shape &first, Shape &second)
{
    std::swap(first.m_coords, second.m_coords);
}

/* defaults to
 * [2]
 * [1][0][3]
 */
J_tile::J_tile()
    : Shape( std::array<Coord, 4>{ Coord{ 0, 0 }, Coord{ 0 , -1 },
                                    Coord{ -1, -1}, Coord{ 0, 1 } })
{
}
