/* Shapes.h
 * Shape objects which are moved in the tetris game. Consists of a list
 * of coordinates.
 */
#ifndef SHAPES_H
#define SHAPES_H

#include <array>

class Coord {
public:
    Coord();
    Coord(const Coord &other);
    Coord(Coord &&other);
    Coord(const double row, const double col);
    Coord& operator=(Coord other);
    
    double& row() { return m_coords[0]; }
    const double& row() const { return m_coords[0]; }
    double& col() { return m_coords[1]; }
    const double& col() const { return m_coords[1]; }

    Coord& operator+=(const Coord &rhs);
    Coord& operator-=(const Coord &lhs);
    
    friend void swap(Coord &first, Coord &second);
private:
    std::array<double, 2> m_coords;
};

Coord operator+(Coord lhs, const Coord &rhs);
Coord operator-(Coord lhs, const Coord &rhs);

class Shape {
public:
    Shape();
    Shape(std::array<Coord, 4> &coords);
    Shape(std::array<Coord, 4> &&coords);
    Shape& operator=(Shape other);
    
    void rotate();
    const std::array<Coord, 4> get_coords() const;

    friend void swap(Shape &first, Shape &second);
protected:
    std::array<Coord, 4> m_coords;
};

class J_tile : public Shape {
public:
    J_tile();
};

#endif 
