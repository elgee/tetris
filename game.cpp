#include "Grid.h"
#include "Shapes.h"
#include "Shape_stack.h"
#include "Player.h"
#include <ncurses.h>
#include <iostream>
#include <chrono>
#include <random>

void display_stack(const Shape_stack &shape_stack, const int start_row, const int start_col);
void display_shape(const Shape_stack &stack);
void display_grid(const Grid &grid);
void clear_shape(const Shape_stack &stack);

void player_setup()
{
}

void game_test()
{
    // ncurses setup
    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    halfdelay(10);

    int cols;
    int rows;
    getmaxyx(stdscr, rows, cols);

    // set up shapes
    Shape J{ std::array<Coord, 4>{ 
        Coord{ 0, 0 }, 
        Coord{ 0, -1 },
        Coord{ -1, -1 },
        Coord{ 0, 1 }
        }
    };
    Shape L{ std::array<Coord, 4> {
        Coord{ 0, 0 },
        Coord{ 0, -1 },
        Coord{ 0, 1 },
        Coord{ 1, 1 }
        }
    };
    Shape T{ std::array<Coord, 4> {
        Coord{ 0, 0 },
        Coord{ 0, -1 },
        Coord{ 0, 1 },
        Coord{ -1, 0 }
        }
    };
    Shape S{ std::array<Coord, 4> {
        Coord{ 0, 0 },
        Coord{ 0, -1 },
        Coord{ 1, 0 },
        Coord{ 1, 1 }
        }
    };
    Shape Z{ std::array<Coord, 4> {
        Coord{ 0, 0 },
        Coord{ 0, 1 },
        Coord{ 1, 0 },
        Coord{ 1, -1 }
        }
    };
    Shape I{ std::array<Coord, 4>{ 
        Coord{ 0, -1.5 },
        Coord{ 0, -0.5 },
        Coord{ 0, 0.5 },
        Coord{ 0, 1.5}
        }
    };
    Shape O{ std::array<Coord, 4>{ 
        Coord{ 0.5, -0.5 }, 
        Coord{ 0.5, 0.5 },
        Coord{ -0.5, 0.5 }, 
        Coord{ -0.5, -0.5} 
        }
    };
    
    std::array<Shape*, 7> shapes { &L, &J, &T, &S, &Z, &O, &I };
    
    // make a random number generator for selecting shapes
    std::random_device dev;
    std::mt19937 random_gen(dev());
    std::uniform_int_distribution<> random_index(0,6);
    
    // player setup
    Player player_one{ 20, 10 };
    Player player_two{ 20, 10 };
    
    player_one.set_key_down('s');
    player_one.set_key_left('a');
    player_one.set_key_right('d');
    player_one.set_key_rotate('w');
    player_two.set_key_down(KEY_DOWN);
    player_two.set_key_left(KEY_LEFT);
    player_two.set_key_right(KEY_RIGHT);
    player_two.set_key_rotate(KEY_UP);
    
    display_stack(player_one.stack(), 0, 0);
    display_stack(player_two.stack(), 0, 12);
    
    Shape_stack& shape_stack = player_one.stack();
//     Shape_stack shape_stack{ 20, 10 };
//     display_stack(shape_stack, 0, 0);
    refresh();
    using namespace std::chrono;
    steady_clock::time_point start;
    duration<double, std::milli> diff;
    while (true) {
        bool at_bottom{ false };
        int index{ random_index(random_gen) };
        switch(index) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                shape_stack.add_shape(*shapes[index], 
                    Coord{ 1, 4 });
                break;
            case 5:
            case 6:
                shape_stack.add_shape(*shapes[index], 
                    Coord{ 1, 4.5 });
                break;
        }
        do {
            start = steady_clock::now();
            do {
                int input;
                input = getch();
                switch (input) {
                    case KEY_LEFT:
                        clear_shape(shape_stack);
                        shape_stack.move_left();
                        break;
                    case KEY_RIGHT:
                        clear_shape(shape_stack);
                        shape_stack.move_right();
                        break;
                    case KEY_DOWN:
                        clear_shape(shape_stack);
                        if (!shape_stack.move_down()) at_bottom = true;
                        break;
                    case KEY_UP:
                        clear_shape(shape_stack);
                        shape_stack.rotate_shape();
                        break;
                }
                display_shape(shape_stack);
                refresh();
                diff = steady_clock::now() - start;
            } while (diff.count() < 1000 && !at_bottom);
            clear_shape(shape_stack);
            at_bottom = !shape_stack.move_down();
            display_shape(shape_stack);
        } while (!at_bottom);
        shape_stack.set_shape_fill(true);
        shape_stack.update_rows();
        display_stack(shape_stack, 0, 0);
        refresh();
    }
    endwin();
}

void grid_test()
{
    // ncurses setup
    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();

    int cols;
    int rows;
    getmaxyx(stdscr, rows, cols);
    Shape j{ std::array<Coord, 4>{ Coord{ 0, 0 }, Coord{ 0, -1 },
                                   Coord{ -1, -1 }, Coord{ 0, 1 }
             }
    };

    Grid grid{ 9, 9 };
    for (int col = 0; col < grid.width(); ++col)
        grid.at(0, col).set_fill(true);
    display_grid(grid);
    refresh();
    getch();
    grid.shift_row_down(0, 8);
    display_grid(grid);
    refresh();
    getch();
    grid.empty_row(8);
    display_grid(grid);
    refresh();
    getch();
    endwin();
}

int main()
{
    game_test();
    return 0;
}

void display_stack(const Shape_stack &stack, const int start_row, const int start_col)
{
    const Grid& grid{ stack.grid() };
    for (int row = 0; row < grid.height(); row++) {
        for (int col = 0; col < grid.width(); col++) {
            if (grid.at(row, col).is_full())
                mvaddch(start_row + row, start_col + col, '#');
            else
                mvaddch(start_row + row, start_col + col, '_');
        }
    }
}

void display_grid(const Grid &grid)
{
    for (int row = 0; row < grid.height(); row++) {
        for (int col = 0; col < grid.width(); col++) {
            if (grid.at(row, col).is_full())
                mvaddch(row, col, ACS_BLOCK);
            else
                mvaddch(row, col, '_');
        }
    }
}

void clear_shape(const Shape_stack &stack)
{
    for (auto &coord : stack.active_shape_coords())
        mvaddch(coord.row(), coord.col(), '_');
} 

void display_shape(const Shape_stack &stack)
{
    for (auto &coord : stack.active_shape_coords())
        mvaddch(coord.row(), coord.col(), '#');
} 


