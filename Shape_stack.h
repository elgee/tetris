/* Shape_stack.h
 * controls where the tiles are on the grid. 
 * The grid is a list of blocks which are either full or not and is
 * to store the stack of place tiles. The active shape is held 
 * seperately and may be moved with various methods. 
 * The active shape does not fill blocks on the grid so that it is easy 
 * to check where the active shape may be positioned and the active shape
 * and stack of blocks may be dealt wth seperately by the front end.
 * Methods:
 * active_shape_coords()
 *      returns the absolute coordinates of the active shape, i.e. the
 *      shape position on the grid plus the relative coordinates of the
 *      shape.
 * const Grid& grid() const 
 *      gets the grid
 * add_shape(const Shape shape, const int row, const int col)
 *      adds a shape centred on the specified row and column making sure
 *      it is within the bounds of the grid.
 * set_shape_fill(bool flag)
 *      sets whether the shape is filled in on the grid which is used to 
 *      create the stack of shapes at the bottom
 * bool can_move_to(const int row, const int col)
 *      tests whether the active shape can be centred on the specified
 *      position on the grid. it returns false if any of the blocks are
 *      out of range or already occupied
 * rotate_shape()
 *      rotates the shape and moves it if it would go off the edge of the
 *      grid. Does nothing if it would rotate into an already filled block
 * update_rows()
 *      checks for complete rows and updates the grid accordingly
 */
#ifndef SHAPE_STACK_H
#define SHAPE_STACK_H

#include "Shapes.h"
#include "Grid.h"
#include <array>

class Shape_stack {
public:
    Shape_stack();
    Shape_stack(const Shape_stack &other);
    Shape_stack(Shape_stack &&other);
    Shape_stack(const int rows, const int cols);
    Shape_stack& operator=(Shape_stack other);
    
    
    std::array<Coord, 4> active_shape_coords() const;
    void set_shape_fill(bool flag);
    bool can_move_to(const Coord &new_coord);
    void move_shape(const Coord &new_coord);
    void move_right();
    void move_left();
    bool move_down();

    bool add_shape(const Shape &shape, const Coord &at_coord);
    void rotate_shape();
    void update_rows();

    const Grid& grid() const { return m_grid; }

    friend void swap(Shape_stack &first, Shape_stack &second);
private:
    Shape m_active_shape;
    Coord m_shape_pos;
    Colour m_shape_colour;
    Grid m_grid;
};

#endif
