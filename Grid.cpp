// Grid.cpp
#include <vector>
#include "Grid.h"
#include "Shapes.h"
#include <exception>

Colour Block::get_colour() const { return m_colour; }
void Block::set_colour(Colour colour) { m_colour = colour; }
bool Block::is_full() const { return m_filled; }
void Block::set_fill(bool flag) {  m_filled = flag; }

Grid::Grid() : m_blocks{}
{
}

Grid::Grid(const Grid &other)
    : m_rows{ other.m_rows },
    m_cols{ other.m_cols }, 
    m_blocks{ other.m_blocks }
{
}

Grid::Grid(Grid &&other) : Grid()
{
    swap(*this, other);
}

Grid::Grid(const int rows, const int cols) 
    : m_rows{ rows }, m_cols{ cols }, m_blocks(static_cast<size_t>(rows * cols))
{
}

Grid& Grid::operator=(Grid other)
{
    swap(*this, other);
    return *this;
}

int Grid::height() const { return m_rows; }
int Grid::width() const { return m_cols; }

Block& Grid::at(const int row, const int col)
{
    if (row >= m_rows || col >= m_cols || row < 0 || col < 0) 
        throw std::out_of_range{ "Index outside of grid." };
        
    return m_blocks.at(row * m_cols + col);
}

const Block& Grid::at(const int row, const int col) const
{
    if (row > m_rows || col > m_cols || row < 0 || col < 0) 
        throw std::out_of_range{ "Index outside of grid." };
        
    return m_blocks.at(row * m_cols + col);
}

bool Grid::is_full(const int row, const int col) const
{
    return this->at(row, col).is_full();
}

void Grid::empty_block(const int row, const int col)
{
    this->at(row, col).set_fill(false);
}

void Grid::empty_row(const int row)
{
    for (int col = 0; col < m_cols; col++)
        empty_block(row, col);
}

// shifting row down increases the row index
void Grid::shift_row_down(const int row, const int shift)
{
    if (shift > m_rows - row - 1 || shift < 0) return;

    for (int col = 0; col < m_cols; col++)
    {
        this->at(row + shift, col) = this->at(row, col);
        this->at(row,col).set_fill(false);
    }
}

void Grid::add_shape(const Shape &shape, const int row, const int col)
{
    for (const Coord &coord : shape.get_coords()) 
        this->at(row + coord.row(), col + coord.col()).set_fill(true);
}

void swap(Grid &first, Grid &second)
{
    std::swap(first.m_rows, second.m_rows);
    std::swap(first.m_cols, second.m_cols);
    std::swap(first.m_blocks, second.m_blocks);
}
